module  app

go 1.12

require (
	github.com/gobuffalo/packr/v2 v2.8.1
	github.com/rakyll/statik v0.1.7
	github.com/zserge/lorca v0.1.8
)
