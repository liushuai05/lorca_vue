# lorca-vue
### 项目依赖
- 本项目打包依赖于statik，开发前请先安装[statik](http://github.com/rakyll/statik)

### 特性

### 特殊说明
- 由于本项目使用goLang调用浏览器内核实现，所以只能运行页面级程序，目前还不支持自定义菜单和常驻任务栏图标。如果需要开发比较复杂的项目，请移步到[electron-vue](https://github.com/SimulatedGREG/electron-vue)
### 调试go+vue

``` bash
#第一次开发需要执行如下命令安装vue依赖
cd vue && yarn && cd ../
#执行如下命令即可打开调试模式
go run main.go -dev
```

### 只调试go程序
``` bash
#linux下打运行
cd vue && yarn build&& cd ../ && statik -src=./vue/dist/ -f  && go run main.go 
```

### 二进制打包
``` bash
#linux下打包
cd vue && yarn build&& cd ../ && statik -src=./vue/dist/ -f&& go build main.go 
```


### 安装包制作
>我只测试了 `build-linux.sh` 
``` bash
#windows使用`build-windows.bat`，生成到`xxx.exe文件`文件，下一步使用“inno setup”或“nsis”打包到“dist”文件夹中。
./cmd/build-windows.bat
#linux跨平台打包windows
./cmd/build-windows.sh
#llinux使用`build-linux.sh版`，生成到`xxx.deb`文件。
./cmd/build-linux.sh

#mac使用`build-macos.sh公司`，生成到`xxx.app`文件。
./cmd/build-macos.sh
```



