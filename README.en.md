# lorca-vue

### debugging goLange+vue
 
``` bash
#For the first development, execute the following command to install Vue dependencies
cd vue && yarn && cd ../
#Execute the following command to turn on debugging mode
go run main.go -dev
```

###  only debugging goLange soft
``` bash
cd vue && yarn build && cd ../ && go run main.go 
```

### binary packing

``` bash
cd vue && yarn build && cd ../ && go build main.go 
```


### Build
> I only test `build-linux.sh`
``` bash
#Windows use `build-windows.bat`,build to `xxx.exe` file, next use `Inno Setup` or `NSIS` packaging with `dist` folder.
./cmd/build-windows.bat
#How to package windows exe program under Linux
./cmd/build-windows.sh
#Linux use `build-linux.sh`,build to `xxx.deb` file.
./cmd/build-linux.sh

#Mac use `build-macos.sh`,build to `xxx.app` file.
./cmd/build-macos.sh
```


